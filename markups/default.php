<?php

$list = ($renderPages->count()) ? $renderPages : $renderPages->append(wire('page'));

$tagName = $settings->tagName;
$color = $settings->color;
$fontSize = $settings->fontSize;
$text = $p->get($settings->field);

foreach ($list as $p) {
  echo "<$tagName style='color:$color;font-size:$fontSize;'>$text</$tagName>";  
}
